<?php

return [
   
    'appFiguresClientKey'   => env('APP_FIGURES_CLIENT_KEY', ''),
    'appFiguresAuth'        => env('APP_FIGURES_AUTH', ''),

    'appleAppId'            => env('APPLE_APP_ID', ''),

    'displayName'          => "App Name",

    // Cannot be greater than 4
    'maxScreenshots'          => 4, 

    'tagline'               => "App's tagline",
    'navLinks'              => [
                                    // ["title" => "Contact", "url" => "mailto:support@me.com"],
        ],

    // Should be an even number of features
    'features'              => [
                                // ["title" => "Feature Name", "body" => "A paragraph about the feature"],
                                ],
    // 'jumbotron'             => ["title" => "A big crazy title", "body" => "A sentence about why I want this app"],

    'gaId'                  => "",

    
    
];
