<?php

namespace Lasmit\AppWebsite;

use Illuminate\Support\ServiceProvider;

class AppWebsiteServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Lasmit\AppWebsite\Http\Controllers\AppWebsiteController');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes/web.php'); 
        $this->publishes([
            __DIR__.'/../config/appwebsite.php' => config_path('appwebsite.php')            
        ]);  
        $this->loadViewsFrom(__DIR__.'/resources/views', 'appwebsite');    
        $this->publishes([__DIR__.'/resources/images' => public_path('vendor/lasmit'),], 'public');         
    }
}
