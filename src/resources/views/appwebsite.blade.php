<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title>{{ $data["seo_name"] }}</title>

<meta name="description" content="{{ $data['seo_description'] }}">

<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<style type="text/css">
	body{
	    text-align: center;
	}
	@media screen and (min-width: 576px){
	    body{
	        text-align: left;
	    }
	}

@if ($data['is_ad'])
	.hide-in-ad { display: none; }
@endif
	.jumbotron { background-color: #2ccbeb; color: white; }

	#squircle{  
	  -webkit-clip-path: url(#svg-shape);
	  -moz-clip-path: url(#svg-shape);
	  -o-clip-path: url(#svg-shape);
	  -ms-clip-path: url(#svg-shape);
	  clip-path: url(#svg-shape);
	}
</style>
</head>
<body>
<div class="container">
	@if ($data['navLinks'])
	<nav class="my-2 mr-1 text-right hide-in-ad">
		@foreach ($data['navLinks'] as $navLink)
		<a class="p-2 text-secondary" href="{{ $navLink['url'] }}">{{ $navLink['title'] }}</a>
		@endforeach
	</nav>
	@endif

	<div class="row my-4">
		<div class="col col-sm-auto">
			<a href="{{ $data['downloadUrl'] }} " target="_blank">
				<img src="{{ url('storage/icon.jpg') }}" width="200" alt="{{ $data['displayName']}} App Icon" class="app-icon" id="squircle" />					
				<svg height="0" width="0" version="1.1" xmlns="http://www.w3.org/2000/svg">
					<defs>
					  <clipPath id="svg-shape" transform="scale(0.390625, 0.390625)">  {{-- ratio = [desired size] 200 / [actual size] 512 = 0.390625 --}}
					  	{{-- the path below was copied and pasted as SVG from the sketch apple app icon template --}}
						<path d="M512,365.858544 C512,416.675112 506.70894,435.102427 496.773436,453.680205 C486.837932,472.257983 472.257983,486.837932 453.680205,496.773436 C435.102427,506.70894 416.675112,512 365.858544,512 L365.858544,512 L146.141456,512 L144.626644,511.998413 C94.9563269,511.893714 76.7117949,506.609585 58.3197949,496.773436 C39.7420171,486.837932 25.1620684,472.257983 15.2265641,453.680205 C5.29105984,435.102427 -1.34577311e-14,416.675112 -1.96809658e-14,365.858544 L-1.96809658e-14,365.858544 L-1.18085795e-14,146.141456 L0.00158667134,144.626644 C0.106285641,94.9563269 5.39041488,76.7117949 15.2265641,58.3197949 C25.1620684,39.7420171 39.7420171,25.1620684 58.3197949,15.2265641 C76.8975727,5.29105984 95.3248882,-2.01865966e-14 146.141456,-2.95214488e-14 L146.141456,-2.95214488e-14 L365.858544,-1.77128693e-14 L367.373356,0.00158667134 C417.043673,0.106285641 435.288205,5.39041488 453.680205,15.2265641 C472.257983,25.1620684 486.837932,39.7420171 496.773436,58.3197949 C506.70894,76.8975727 512,95.3248882 512,146.141456 L512,146.141456 Z" id="path-1"></path>
				  	</clipPath>				  	
				    </defs>
				</svg>	

			</a>
		</div>	
		<div class="col-sm-auto">
			<h1>{{ $data['displayName']}}</h1>			
			<p class="lead">{{ $data['tagline'] }}</p>

			@if ($data['ratings_average'] > 3)
    
			<div class="align-baseline">
    
    			{{-- I'm not proud of this, but it works --}}
	        	<img height="15" src='vendor/lasmit/star.fill.svg'><img height="15" src='vendor/lasmit/star.fill.svg'><img height="15" src='vendor/lasmit/star.fill.svg'>@if ($data['ratings_average'] >= 3 && $data['ratings_average'] <= 3.75)<img height="15" src='vendor/lasmit/star.lefthalf.fill.svg'><img height="15" src='vendor/lasmit/star.svg'>@elseif ($data['ratings_average'] >= 3.76 && $data['ratings_average'] <= 4.25)<img height="15" src='vendor/lasmit/star.fill.svg'><img height="15" src='vendor/lasmit/star.svg'>@elseif ($data['ratings_average'] >= 4.26 && $data['ratings_average'] <= 4.75)<img height="15" src='vendor/lasmit/star.fill.svg'><img height="15" src='vendor/lasmit/star.lefthalf.fill.svg'>@else<img height="15" src='vendor/lasmit/star.fill.svg'><img height="15" src='vendor/lasmit/star.fill.svg'>@endif

				<p>{{ number_format($data['ratings_average'], 1) }} stars - {{ $data['ratings_total'] }} Worldwide Ratings</p>
			</div>			
			@endif

		</div>
	</div>

	<div class="row text-center d-sm-none">
		<div class="col">
			<a href="{{ $data['downloadUrl'] }} " target="_blank"><img class="my-4" width="220" src="{{ url('vendor/lasmit/download-app-store.svg') }}" title="Download on the App Store" /></a>
		</div>
	</div>

	@if ($data['jumbotron'])
	<div class="jumbotron">
		<h2>{{ $data['jumbotron']['title'] }}</h2>
		<p class="lead">{{ $data['jumbotron']['body'] }}</p>
	</div>
	@endif

	@if ($data['screenshots'])
	<div class="row">
		@foreach ($data['screenshots'] as $screenshot)
		<div class="col text-center">
			<a href="#" target="_blank">
				<img src="{{ url('storage/' . $screenshot) }}" width="220px" class="m-2 rounded shadow border-0">
			</a>
		</div>		
		@endforeach
	</div>
	@endif


	@if ($data['features'])
	<div class="row text-center">
		<div class="col">
			<a href="{{ $data['downloadUrl'] }} " target="_blank"><img class="my-4" width="220" src="{{ url('vendor/lasmit/download-app-store.svg') }}" title="Download on the App Store" /></a>
		</div>
	</div>

	@foreach ($data['features'] as $feature)
	@if ($loop->odd)	
	<div class="row">
	@endif

		<div class="col-sm-6" id="{{ $loop->iteration }} {{ $loop->odd }}">
			<h4>{{ $feature['title'] }}</h4>
			<p>{{ $feature['body'] }}</p>
		</div>

	@if ($loop->even)
	</div> <!-- end of row -->
	@endif

	@endforeach
	
	<!-- end of loop -->
	</div>
	@endif


	<div class="row text-center">
		<div class="col">
			<a href="{{ $data['downloadUrl'] }} " target="_blank"><img class="my-4" width="220px" src="{{ url('vendor/lasmit/download-app-store.svg') }}" title="Download on the App Store" /></a>
		</div>
	</div>


	@if ($data['navLinks'])
	<nav class="my-6 my-md-0 mr-md-3 text-center hide-in-ad">
		@foreach ($data['navLinks'] as $navLink)
		<a class="p-2 text-secondary" href="{{ $navLink['url'] }}">{{ $navLink['title'] }}</a>
		@endforeach
	</nav>
	@endif

	<footer class="text-center mb-4 hide-in-ad">
		&copy; {{  date('Y') }} {{ $data["companyName"] }}  - <a href="{{ $data['moreAppsUrl']}}">More apps by {{ $data["companyName"] }}</a>
	</footer>


</div>

<!-- GA -->
</body>
</html>