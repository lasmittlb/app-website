<?php

namespace Lasmit\AppWebsite\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use \Storage;
use Carbon\Carbon;
use \Cache;
use Imagick;

class AppWebsiteController extends Controller
{
    public function index(Request $request) {
   
   		$this->fetchIcon(Carbon::now()->addDays(7));
   	
   		$this->fetchRatings(Carbon::now()->addDays(1));
		
   		$this->fetchScreenshots(Carbon::now()->addDays(2));
   

   
   		$screenshots = [];

   		$storage = Storage::disk('public');
    	
    	for ($i=0; $i < min(config('appwebsite.maxScreenshots'), 4); $i++) { 
    		$fileName = "screenshot-".$i.".jpg";
			if ($storage->exists($fileName)) {
				$screenshots[] = $fileName;
			} else {
				break;
			}
		}

		
   		$data = [
   			"is_ad" => $request->input('is_ad') == true,
   			"seo_name" => Cache::get('apple.name'),
   			"seo_description" => Cache::get('apple.description'),
   			"ratings_total" => $this->thousandsFormat(Cache::get('ratings.total')),
   			"ratings_average" => Cache::get('ratings.average'),
   			"navLinks" => config('appwebsite.navLinks'),
   			"features" => config('appwebsite.features'),
   			"jumbotron" => config('appwebsite.jumbotron'),
   			"gaId" => config('appwebsite.gaId'),	
   			"displayName" => config('appwebsite.displayName'),
   			"tagline" => config('appwebsite.tagline'),
	   		"moreAppsUrl" => Cache::get('company.apps'),
   			"companyName" => Cache::get('company.name'),
   			"downloadUrl" => 'https://apps.apple.com/gb/app/id' . Cache::get('apple.id'),
   			"screenshots" => $screenshots,
   		];
       
    	return view('appwebsite::appwebsite', ["data" => $data]);
    }

    private function thousandsFormat($num) {

		if ($num>1000) {

	        $x = round($num);
	        $x_number_format = number_format($x);
	        $x_array = explode(',', $x_number_format);
	        $x_parts = array('k', 'm', 'b', 't');
	        $x_count_parts = count($x_array) - 1;
	        $x_display = $x;
	        $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
	        $x_display .= $x_parts[$x_count_parts - 1];

	        return $x_display;
		}

		return $num;
	}

    private function fetchRatings($expiresAt) {
    	
    	if (Cache::has("ratings.average")) {
       		return;
       	}

    	$ch = curl_init();

    	$dateString = date("Y-m-d");

	    // $countries = "AF,AL,DZ,AS,AD,AO,AI,AQ,AG,AR,AM,AW,AU,AT,AZ,BS,BH,BD,BB,BY,BE,BZ,BJ,BM,BT,BO,BA,BW,BV,BR,VG,IO,BN,BG,BF,BI,KH,CM,CA,CV,KY,CF,TD,CL,CN,HK,MO,CO,KM,CG,CD,CK,CR,CI,HR,CU,CY,CZ,DK,DJ,DM,DO,EC,EG,SV,GQ,ER,EE,ET,FK,FO,FJ,FI,FR,GF,PF,TF,GA,GM,GE,DE,GH,GI,GR,GL,GD,GP,GU,GT,GG,GN,GW,GY,HT,VA,HN,HU,IS,IN,ID,IR,IQ,IE,IM,IL,IT,JM,JP,JE,JO,KZ,KE,KI,KR,KW,KG,LA,LV,LB,LS,LR,LY,LI,LT,LU,MK,MG,MW,MY,MV,ML,MT,MH,MQ,MR,MU,YT,MX,FM,MD,MC,MN,ME,MS,MA,MZ,MM,NA,NR,NP,NL,AN,NC,NZ,NI,NE,NG,NU,NF,MP,NO,OM,PK,PW,PS,PA,PG,PY,PE,PH,PL,PT,PR,QA,RE,RO,RU,RW,KN,LC,VC,WS,SM,ST,SA,SN,RS,SC,SL,SG,SK,SI,SB,SO,ZA,GS,ES,LK,SD,SR,SZ,SE,CH,SY,TW,TJ,TZ,TH,TL,TG,TK,TO,TT,TN,TR,TM,TC,TV,UG,UA,AE,GB,US,UY,UZ,VU,VE,VN,VI,WF,YE,ZM,ZW";

	    // $countries = "AE"; // for a smaller dataset for testing

		$appId = Cache::get("appfigures.id");  // fetched in fetchAppIcon()
		$secret = config('appwebsite.appFiguresClientKey');
		$auth = config('appwebsite.appFiguresAuth');
		
		$url = 'https://api.appfigures.com/v2/reports/ratings?products=' . $appId . '&client_key=' . $secret .
	        '&start_date='. $dateString . '&end_date=' . $dateString;//  . 
	        // '&countries=' . $countries;

	    // dd($url);

	    curl_setopt($ch, CURLOPT_URL, $url);

	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, [
	      'Authorization: Basic ' . $auth,
	    ]);

	    $response = curl_exec($ch);

	    if (!$response) {
	    	return;	      
	    }

	    curl_close($ch);

	    // var_dump($response);

	    $result = json_decode($response, true);

	    Cache::put("ratings.total", $result['total'], $expiresAt);  
	    Cache::put("ratings.average", $result['average'], $expiresAt);  
    }

    private function fetchScreenshots($expiresAt) {
    	
    	if (Cache::has("screenshots")) {
       		return;
       	}

		$storage = Storage::disk('public');
    	
		$ch = curl_init();

		$appId = config('appwebsite.appleAppId');
		
		$url = 'https://apps.apple.com/gb/app/id' . $appId;

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
		$response = curl_exec($ch);
		if (!$response) {
			$self->statusCode = 500;
			return $self->respondWithError("Error at source");
		}
	
		preg_match_all('/<script name="schema:software-application" .*>(.*)/', $response, $output_array);
		curl_close($ch);
		
		$data = json_decode($output_array[1][0], true); 

		for ($i=0; $i < min(config('appwebsite.maxScreenshots'), 4); $i++) { 
			if (isset($data['screenshot'][$i])) {
				$storage->put("screenshot-".$i.".jpg", file_get_contents($data['screenshot'][$i]));				
			}
		}
		
		Cache::put("screenshots", true, $expiresAt);  

		// Set the cache on these far in the future so that we still have data even if the above fails
		Cache::put("apple.name", $data["name"], Carbon::now()->addDays(365));
		Cache::put("apple.description", $data["description"], Carbon::now()->addDays(365));		
		Cache::put("company.name", $data["author"]["name"], Carbon::now()->addDays(365));		
		Cache::put("company.apps", $data["author"]["url"], Carbon::now()->addDays(365));		
    }

    private function fetchIcon($expiresAt) {

		if (Cache::has("icon")) {
       		return;
       	}

    	$storage = Storage::disk('public');	
	
		$ch = curl_init();


		$appId = config('appwebsite.appleAppId');
		$secret = config('appwebsite.appFiguresClientKey');
		$auth = config('appwebsite.appFiguresAuth');
		
		$url = 'https://api.appfigures.com/v2/products/apple/' . $appId . '?client_key=' . $secret;

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
		'Authorization: Basic ' . $auth, // can figure this out by making a request in paw. seems safer than putting username and password into config
		]);
		$response = curl_exec($ch);
		if (!$response) {
			$self->statusCode = 500;
			return $self->respondWithError("Error at source");
		}
	
		$data = json_decode($response, true); 

		Cache::put("appfigures.id", $data['id'], Carbon::now()->addDays(365)); 

		$iconUrl = $data['icon'];
		
		curl_close($ch);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $iconUrl);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);

		if (!$response) {
		  return;
		}

		curl_close($ch);

		$storage->put("icon.jpg", $response);

		Cache::put("icon", true, $expiresAt);  
    }

}