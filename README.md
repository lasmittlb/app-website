## Install Instructions

`laravel new myapp`
`cd myapp`

open `routes/web.php` and remove the default route

`composer require lasmit/appwebsite`
`php artisan vendor:publish`
choose `Lasmit\AppWebsite\AppWebsiteServiceProvider`
`nano config/appwebsite.php`


To clear cache:

`php artisan cache:clear`